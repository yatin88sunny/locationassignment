package com.example.sunny.lrslocation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

public class SplashScreen extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try{
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            setContentView(R.layout.activity_spalsh_screen);
            //code that displays the content in full screen mode
            Thread background = new Thread() {
                public void run() {
                    try {
                        // Thread will sleep for 5 seconds
                        sleep(3*1000);
                        // After 5 seconds redirect to another intent
                        Intent i=new Intent(getBaseContext(),LoginActivity.class);
                        startActivity(i);
                        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
                        //Remove activity
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            // start thread
            background.start();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
