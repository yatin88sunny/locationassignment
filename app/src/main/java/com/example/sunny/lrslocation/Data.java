package com.example.sunny.lrslocation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.sunny.app.AppConfig;
import com.example.sunny.helper.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

public class Data extends AppCompatActivity {
    private Button btnMAP, btnSignOut;
    public EditText edtTime, edtMillisec, edtDate;
    private SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try{
            this.requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_data);
            edtTime = (EditText)findViewById(R.id.edtTime);
            edtMillisec = (EditText)findViewById(R.id.edtMilliseconds);
            edtDate = (EditText)findViewById(R.id.edtDate);
            getData();
            session = new SessionManager(getApplicationContext());
            btnMAP = (Button)findViewById(R.id.btnMap);
            btnSignOut = (Button)findViewById(R.id.btnSignOut);
            btnMAP.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Data.this, MapsActivity.class);
                    startActivity(intent);
                }
            });
            btnSignOut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    session.setLogin(false);
                    Intent intent = new Intent(Data.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void getData(){
        try {
            final String __strFinalURL = AppConfig.URL_VIEW;
            StringRequest stringRequest = new StringRequest(__strFinalURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jObj = new JSONObject(response);
                                String gotTime = jObj.getString("time");
                                String gotMillisec = jObj.getString("milliseconds_since_epoch");
                                String gotDate = jObj.getString("date");
                                edtTime.setText(gotTime);
                                edtMillisec.setText(gotMillisec);
                                edtDate.setText(gotDate);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(Data.this, error.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
