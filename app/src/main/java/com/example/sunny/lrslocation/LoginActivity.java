package com.example.sunny.lrslocation;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.sunny.app.AppConfig;
import com.example.sunny.connection.ConnectionDetector;
import com.example.sunny.globalvalues.GlobalData;
import com.example.sunny.gpstracker.GPSTracker;
import com.example.sunny.helper.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends Activity {
    private static final String TAG = RegisterActivity.class.getSimpleName();
    private Button btnLogin;
    private Button btnLinkToRegister;
    private EditText inputEmail;
    private EditText inputPassword;
    private ProgressDialog pDialog;
    private SessionManager session;
    GlobalData gd;
    private static final String TAG_ReceiverId = "ID";
    private static final String TAG_User = "user";
    ConnectionDetector con;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        con = new ConnectionDetector(LoginActivity.this);
        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLinkToRegister = (Button) findViewById(R.id.btnLinkToRegisterScreen);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // Session manager
        session = new SessionManager(getApplicationContext());
        GPSTracker gpsTracker = new GPSTracker(LoginActivity.this);
        if (!gpsTracker.canGetLocation()) {
            gpsTracker.showSettingsAlert();
        }
        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(LoginActivity.this, Data.class);
            startActivity(intent);
            finish();
        }
        // Login button Click Event
        btnLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                String email = inputEmail.getText().toString();
                String password = inputPassword.getText().toString();
                if (con.isConnectingToInternet()) {
                    // Check for empty data in the form
                    if (email.trim().length() > 0 && password.trim().length() > 0) {
                        // login user
                        checkLogin(email, password);
                       /* Intent i = new Intent(getApplicationContext(), MapsActivity.class);
                        startActivity(i);
                        finish();*/
                    } else {
                        // Prompt user to enter credentials
                        Toast.makeText(getApplicationContext(), "Please enter the credentials!", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplication(), "Please Check the connection!!!", Toast.LENGTH_SHORT).show();
                }
            }

        });

        // Link to Register Screen
        btnLinkToRegister.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),
                        RegisterActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

    private void checkLogin(final String email, final String password) {
        // Tag used to cancel the request
        String tag_string_req = "req_login";

        pDialog.setMessage("Logging in ...");
        showDialog();
        try {
            final String __strFinalURL = AppConfig.URL_LOGIN;
            StringRequest stringRequest = new StringRequest(__strFinalURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            hideDialog();
                            try {
                                JSONObject jObj = new JSONObject(response);
                                String gotUsername = jObj.getString("username");
                                String gotKey = jObj.getString("key");
                                if(email.matches(gotUsername)){
                                    if(password.matches(gotKey)){
                                        session.setLogin(true);
                                        Intent intent = new Intent(LoginActivity.this,
                                                Data.class);
                                        startActivity(intent);
                                        finish();
                                    } else {
                                        Toast.makeText(LoginActivity.this, "Username and Password Does not match!!!", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(LoginActivity.this, "Username and Password Does not match!!!", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                Log.d(TAG, "Response: " + response.toString());
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            hideDialog();
                            Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

            /*One can use this code if parameters has to send in map format
              commented by :- yatin gosavi*/


            /*StringRequest strReq = new StringRequest(Request.Method.POST,AppConfig.URL_LOGIN, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d(TAG, "Login Response: " + response.toString());
                    hideDialog();

                    try {
                        JSONObject jObj = new JSONObject(response);
                        String UserId = jObj.getString(TAG_User);
                        JSONObject __jobjID = new JSONObject(UserId);
                        Intent intent = new Intent(LoginActivity.this,
                                MapsActivity.class);
                        startActivity(intent);
                        finish();
                    } catch (JSONException e) {
                        Log.d(TAG, "Response: " + response.toString());
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "Login Error: " + error.getMessage());
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    hideDialog();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters to login url
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("action", "auth_user");
                    params.put("username", email);
                    params.put("key", password);
                    return params;
                }
            };
            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);*/
        } catch (Exception e) {
            e.printStackTrace();
            hideDialog();
            Toast.makeText(LoginActivity.this, "Null Point Exception", Toast.LENGTH_SHORT).show();
        }
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
