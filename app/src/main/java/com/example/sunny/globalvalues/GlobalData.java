package com.example.sunny.globalvalues;

/**
 * Created by llama on 7/10/2015.
 */
public class GlobalData {
    public static String abc, UserID;

    public static String getAbc() {
        return abc;
    }

    public static void setAbc(String abc) {
        GlobalData.abc = abc;
    }

    public static String getUserID() {
        return UserID;
    }

    public static void setUserID(String userID) {
        UserID = userID;
    }
}
